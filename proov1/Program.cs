﻿using System;

namespace proov1
{
    class Program
    {
        static void Main(string[] args)
        {
            var eesnimi = "Tanel";
            var perenimi = "Tumanov";

            var taisNimi = eesnimi + " " + perenimi;
            var myFullName = string.Format("Minu nimi on {0} {1}", eesnimi, perenimi);

            var names = new string[3] { "Isand", "Emand", "Hoor" };
            var formattedNames = string.Join(", ", names);

            Console.WriteLine(formattedNames);

            var text = @"tere jõudu kuidas läheb 
mis vaatad";
            Console.WriteLine(text);
            Console.WriteLine("Hello World ja pane see käima!");
        }
    }
}
